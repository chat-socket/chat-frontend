export const baseUrl = 'http://localhost:3000';

export const postRequest = async (url: string, body: any) => {
  const response = await fetch(`${baseUrl}/${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body,
  });

  const data = await response.json();
  if (!response.ok) {
    let message;

    if (data?.success) {
      message = data.message;
    } else {
      message = data;
    }
    return { error: true, message };
  }

  return data;
};

export const getRequest = async (url: string, isAuth: boolean = true) => {
  console.log({ isAuth });
  const object = {};
  if (isAuth) {
    const user = localStorage.getItem('User') as any;
    const token = JSON.parse(user)?.token;
    console.log({ user });
    Object.assign(object, { headers: { Authorization: `Bearer ${token}` } });
  }
  const response = await fetch(`${baseUrl}/${url}`, object);

  const data = await response.json();
  if (!response.ok) {
    let message;

    if (data?.success) {
      message = data.message;
    } else {
      message = data;
    }
    return { error: true, message };
  }

  return data;
};
